# pg_SQL_Dev_plsql_Colors

Code Editor changes for Oracle SQL Developer after applying the darkmode components suggested by Kris Rice (https://twitter.com/krisrice/status/1210182654911504386)


Instructions 
Use the same instructions as defined by ozbsidian found here (https://github.com/ozmoroz/ozbsidian-sqldeveloper/blob/master/README.md)

Note.  If you are using PortableApps (like I do) then you'll need to look in the directory that is defined in your sqldeveloper.conf file instead of the the AppData folder
